# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed



REPORT
====

| **Basic Item**                                   | **Finished**|
| :----------------------------------------------: | :-------:   |
| Brush and eraser                                 |     O       |
| Color selector                                   |     O       |
| brush size                                       |     O       |
| type text                                        |     O       |
| Font menu (size and typeface )                   |     O       |
| Cursor icon                                      |     O       |
| Reset canvas                                     |     O       |
 


| **Advance tools**                                | **Finished**|
| :----------------------------------------------: | :-------:   |
| Circle, rectangle and triangle                   |     O       |
| Un/Re-do button                                  |     O       |
| Upload image                                     |     O       |
| Download current canvas                          |     O       |

Function demonstration
----
**Function icon**
    採用GOOGLE ICON
    `<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">`
    和 W3SCHOOL的ICON
   ` <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">`
**button click**
    所有案件觸發都是使用addEventListener
```
document.getElementById('square').addEventListener('click', function() {
    ...
}, false);
```
**Function choose**
    採用flag看現在要執行哪種功能
    
```
var pen_flag = 0;
var circle_flag = 0;
var rect_flag= 0;
var tri_flag = 0;
var text_flag =0;
```

**Brush and eraser**
    Brush:偵測滑鼠，按下之後開始畫並偵測滑鼠move，等按鍵鬆開才結束。
```
canvas.addEventListener('mousedown', function(evt) {
	var mousePos = getMousePos(canvas, evt);

    ...
	if(pen_flag == 1){
		
		ctx.beginPath();
		ctx.moveTo(mousePos.x, mousePos.y);
		evt.preventDefault();
		canvas.addEventListener('mousemove', mouseMove, false);
	}
});
		
canvas.addEventListener('mouseup', function(evt) {
    var mousePos = getMousePos(canvas, evt);
    ...
    canvas.removeEventListener('mousemove', mouseMove, false);

}, false);
```

**Color selector**
偵測到某個顏色被按下的時候，就把筆的顏色改成該顏色。
```
var colors = ['red', 'blue', 'green', 'purple', 'yellow', 'orange', 'pink', 'black', 'white', 'grey'];
	var sizeNames = ['default', 'three', 'five', 'ten', 'fifteen', 'twenty'];

	function listener(i) {
	  document.getElementById(colors[i]).addEventListener('click', function() {
		ctx.strokeStyle = colors[i];
	  }, false);
		}

	for(var i = 0; i < colors.length; i++) {
	  listener(i);
}
```

**brush size**
    使用scroller偵測大小(1~80)
```
<div class="size_scroller" >
		<p id= size_title>Brush Size:</p>
	<br><input id="pen_size" type="range" name="points" min="1" max="80" value="0" ><br>
</div>
```

    再去更改筆寬
    
```
$('.size_scroller input').change( function(){
	size = $('#pen_size').val();
	console.log(size);
	console.log(txt_font);
	ctx.lineWidth = size;
});
```

**type text and Font menu (size and typeface )**  
輸入方法: (1)先按下輸入文字的按紐 (2)左方方格中輸入要輸入的文字 (3)使用拉條調整大小並選擇想要的字形 (5) 在畫布上點想要的位置後，按提交。 (也可以直接按提交，格式就會採用預設)

實作方式是用scroller紀錄大小(txt_size)，選單紀錄字形，格子紀錄input(TXT)，當提交(text_btn)被按下的時候，就會採用那些輸入輸出文字
```
var INPUT_text = document.getElementById("TXT");
var INPUT_btn = document.getElementById("text_btn");
	text_btn.onclick = function() {
    //clear
    
var value = INPUT_text.value;
if(text_flag == 1) {
	ctx.font = txt_size +"px "+ txt_font;
	ctx.fillText( value, text_x, text_y);
}
```
註:因為有使用text_flag所以一定要先按下文字按鈕後，才能輸入。

**Cursor icon**
    在案件觸發的同時對cursor做更改
```
document.getElementById('text').addEventListener('click', function() {
	var x = document.getElementById("art");
	x.style.cursor = "url(http://www.rw-designer.com/cursor-extern.php?id=84490) , default";
}, false);
```

    各按鍵的URL:
    筆:url(http://www.rw-designer.com/cursor-extern.php?id=12191)
    擦布:url('http://www.rw-designer.com/cursor-extern.php?id=72976')
    圓:url(http://www.rw-designer.com/cursor-extern.php?id=88899)
    長方形:url(http://www.rw-designer.com/cursor-extern.php?id=96311) 
    三角形:url(http://www.rw-designer.com/cursor-extern.php?id=84490)
    文字輸入:url(http://www.rw-designer.com/cursor-extern.php?id=90809)
    
**Reset canvas**
偵測到reset就直接使用clearRect，清除整張畫布。
```
document.getElementById('reset').addEventListener('click', function() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    console.log("RESET");
}, false);
```

Advance tools
---
**Circle, rectangle and triangle** 
先記錄一開始滑鼠按下的位置，等鬆開滑鼠按鍵的時候看當前位置跟一開始位置相差多少，用差距值決定要畫多大的圖形
使用方法: 
(1)圓形:按下後，滑鼠往右移圓變大，左移圓變小。
(2)方形和三角形:按下後，往右移寬度變大，左移寬度變小，往上移高度變低，往下移高度變高。

Circle:
紀錄原始位置:cir_x =0, cir_y =0，使用arc()畫。
		
```
if(circle_flag == 1){
		
	ctx.beginPath();
	var size = (circle_radius+mousePos.x - cir_x)>0?circle_radius+mousePos.x - cir_x:0;
	ctx.arc(cir_x , cir_y,  size , 0, 2 * Math.PI);
	ctx.closePath();
	ctx.stroke();
}
```

Rectangle and Triangle:

紀錄原始位置:tri_rect_x, tri_rect_y 
使用rect()畫長方形，lineTo()畫triangle()。
```
if{...}
else if(rect_flag == 1){
	canvas.addEventListener('mousemove', mouseMove, false);
	ctx.beginPath();
	var size_x = ( rec_size + mousePos.x - tri_rect_x)>0? rec_size+mousePos.x - tri_rect_x:0;
	var size_y = ( rec_size + mousePos.y - tri_rect_y)>0? rec_size+mousePos.y - tri_rect_y:0;
	ctx.rect(tri_rect_x, tri_rect_y, size_x, size_y);
	ctx.closePath();
	ctx.stroke();
}else if( tri_flag == 1){
	canvas.addEventListener('mousemove', mouseMove, false);
	ctx.beginPath();

	var size_x = ( tri_size + mousePos.x - tri_rect_x)>0? tri_size+mousePos.x - tri_rect_x:0;
	var size_y = ( tri_size + mousePos.y - tri_rect_y)>0? tri_size+mousePos.y - tri_rect_y:0;
	ctx.moveTo( tri_rect_x + size_x, tri_rect_y);
	ctx.lineTo( tri_rect_x, tri_rect_y + size_y);
	ctx.lineTo( tri_rect_x + 2*size_x, tri_rect_y + size_y);
	ctx.fill();
	ctx.closePath();
}
```

**Un/Re-do button**
滑鼠鬆開時用push把畫部記錄在history內，按undo跟redo時用step去存取history，並輸出圖像。
```
function push() {
	step++;
	if (step < history.length - 1) {
		history.length = step + 1
	}
	console.log("PUSH step: "+step);
	history.push(canvas.toDataURL()); //放入陣列
}
```

```
function undo() {
	console.log("current step"+step);
	if (step > 0) {
		step--;
		let canvaspic = new Image(); //建立新Image
		canvaspic.src = history[step]; //載入影像
		canvaspic.onload = function() {
			ctx.clearRect(0, 0, canvas.width, canvas.height);
			ctx.drawImage(canvaspic, 0, 0) //匯出
		}
	}
}
function redo() {
	console.log("current step"+step);
	if (step < history.length -1 ) {
		step++;
		let canvaspic = new Image(); //建立新Image
		canvaspic.src = history[step]; //載入影像
		canvaspic.onload = function() {
			ctx.clearRect(0, 0, canvas.width, canvas.height);
			ctx.drawImage(canvaspic, 0, 0) //匯出
		}
	}
}
```


**Upload image**
使用原本就有的原件，上傳檔案。

<input type="file" id="imageLoader"/>
偵測上傳檔案，上傳完成後就將之輸出到畫布。

```
var imageLoader = document.getElementById('imageLoader');
imageLoader.addEventListener('change', handleImage, false);

function handleImage(e){
	var reader = new FileReader();
	reader.onload = function(event){
			var img = new Image();
			img.onload = function(){
					ctx.drawImage(img,0,0);
			}
			img.src = event.target.result;
	}
	reader.readAsDataURL(e.target.files[0]);     
}
```

**Download current canvas**

直接採用html內建下載語法。

`<a href="#" download="painter.png" onclick="this.href=canvas.toDataURL();" >Download this Graph</a>`